import {requestApi} from "./NetworkService";

export async function getProjects() {
    return await requestApi('/projects');
}

export async function getProject(projectId) {
    return await requestApi('/projects/' + projectId);
}

export async function createProject(data) {
    return await requestApi('/projects', "POST", data);
}

export async function getSteps() {
    return await requestApi('/steps');
}

export async function getStep(stepId) {
    return await requestApi('/steps/' + stepId);
}
