import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Toast from 'react-native-toast-message'
import AppNav from "./navigation/AppsNav";

import {
    QueryClient,
    QueryClientProvider,
} from 'react-query'

// Create a client
const queryClient = new QueryClient()

export default function App() {
  return (
    <>
        <QueryClientProvider client={queryClient}>
          <NavigationContainer>
            <AppNav />
          </NavigationContainer>
          <Toast ref={(ref) => Toast.setRef(ref)} />
        </QueryClientProvider>
    </>
  );
}
