import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../screens/Home';
import Project from '../screens/Project';
import CreateProject from '../screens/CRUD/create/CreateProject';
import {Button} from "react-native";
import {FontAwesome} from "@expo/vector-icons";

const Stack = createStackNavigator();

const AppNav = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="Home"
                component={Home}
                options={{
                    title: 'Mes projets',
                    headerStyle: {
                        backgroundColor: '#214342'
                    },
                    headerTintColor: '#214342',
                    headerTitleAlign: 'center',
                    headerTitleStyle: {
                        color: 'white',
                        fontWeight: 'bold',
                    },
                }}
            />
            <Stack.Screen
                name="Project"
                component={Project}
                options={{
                    title: 'Projet',
                    headerStyle: {
                        backgroundColor: '#214342'
                    },
                    headerTintColor: '#214342',
                    headerTitleAlign: 'center',
                    headerTitleStyle: {
                        color: 'white',
                        fontWeight: 'bold',
                    },
                }}
            />
            <Stack.Screen
                name="CreateProject"
                component={CreateProject}
                options={{
                    title: 'Créer un projet',
                    headerStyle: {
                        backgroundColor: '#214342'
                    },
                    headerTintColor: '#214342',
                    headerTitleAlign: 'center',
                    headerTitleStyle: {
                        color: 'white',
                        fontWeight: 'bold',
                    },
                    headerBackImage: ()=>(
                        <FontAwesome
                            name="arrow-left"
                            size={22}
                            color={"white"}
                        />
                    ),
                }}
            />
        </Stack.Navigator>
    );
}

export default AppNav;
