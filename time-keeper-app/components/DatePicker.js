import {StyleSheet, Text, TextInput, View} from "react-native";
import React from "react";

const Datepicker = props => {

    const {handleChange, handleBlur, values} = props

    const checkValue = (type, value) => {
        const thisYear = new Date().getFullYear();

        switch (type) {
            case 'date':
                return value > 0 && value <= 31;
            case 'month':
                return value > 0 && value <= 12;
            case 'year':
                return value >= thisYear;
        }
    }

    const onChange = (value, type) => {

        if(checkValue(type, value)) {
            handleChange(type);
        }
    }

    return(
        <View style={styles.datepicker}>
            <TextInput
                keyboardType='numeric'
                numeric
                placeholder="01"
                onChangeText={date => onChange(date, 'date')}
                onBlur={handleBlur('date')}
                value={values.date}
                style={styles.inputDate}
                maxLength={2}
                minLength={2}
            />
            <Text>/</Text>
            <TextInput
                keyboardType='numeric'
                placeholder="01"
                numeric
                onChangeText={date => onChange(date, 'month')}
                onBlur={handleBlur('month')}
                value={values.month}
                style={styles.inputDate}
                maxLength={2}
                minLength={2}
            />
            <Text>/</Text>
            <TextInput
                keyboardType='numeric'
                placeholder="2000"
                numeric
                onChangeText={date => onChange(date, 'year')}
                onBlur={handleBlur('year')}
                value={values.year}
                style={styles.inputDate}
                maxLength={4}
                minLength={4}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    inputDate: {
        height: 40,
        margin: 12,
        padding: 10,
    },
    datepicker: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }
})

export default Datepicker;