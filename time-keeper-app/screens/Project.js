import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {Button, Icon, Text} from "react-native-elements";
import {useQuery} from "react-query";
import {getProject, getSteps} from "../service/ApiService";
import { milisecondToShowFormat } from "../service/TimeFormatterService";

const Project = ({ route, navigation }) => {

    const { projectId } = route.params;
    const [clickedStep, setClickedStep] = useState(0);

    const projectQuery = useQuery(['project', projectId], () => getProject(projectId));
    const stepsQuery = useQuery('steps', () => getSteps());

    if (projectQuery.isLoading || stepsQuery.isLoading) {
        return <View style={styles.container}>
            <Button
                buttonStyle={styles.button}
                title="Loading button"
                loading
            />
        </View>
    }
    // format datas to showing object
    const projet = projectQuery.data.data
    const steps = stepsQuery.data.data

    function calcTotalPlannedTime(projetParam) {
        let totalTime = 0;
        projetParam.steps.forEach(step => {
            totalTime = totalTime + step.timePlanned;
        })
        return totalTime;
    }

    function calcTotalSpendedTime(steps) {
        let timeSpended = 0;
        steps.forEach(step => {
            if(step.chronos.length > 0) {
                step.chronos.forEach( chrono => {
                    timeSpended = timeSpended + chrono.duration;
                });
            }
        });
        return timeSpended;
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>
                {projet.name}
            </Text>
            <Text style={styles.info}>
                Temps total prévu : { milisecondToShowFormat(calcTotalPlannedTime(projet)) }
            </Text>
            <Text style={styles.info}>
                Temps total passé : { milisecondToShowFormat(calcTotalSpendedTime(steps)) }
            </Text>
            <Text style={styles.info}>
                Date limite : {projet.deadline}
            </Text>
            <Text style={styles.info}>
                Nombre d'étapes : {projet.steps.length}
            </Text>
            <Button
                icon={
                    <Icon
                        name="add"
                        size={15}
                        color="white"
                    />
                }
                buttonStyle={styles.buttonAddStep}
                title="Ajouter une étape"
                onPress={() => {
                    setClickedStep(step.id)
                }}
            />

            {
                projet.steps.map(step => {
                    if (step.id === clickedStep) {
                        return (
                            <>
                                <Button
                                    icon={
                                        <Icon
                                            name="arrow-right"
                                            size={15}
                                            color="white"
                                        />
                                    }
                                    buttonStyle={styles.button}
                                    key={step.id}
                                    title={step.name}
                                    onPress={() => {
                                        setClickedStep(step.id)
                                    }}
                                />
                                <Button
                                    buttonStyle={styles.start}
                                    title='start'
                                    icon={
                                        <Icon
                                            name="clock"
                                            type="foundation"
                                            size={15}
                                            color="white"
                                        />
                                    }
                                />
                            </>
                        )

                    } else {
                        return (
                            <Button
                                icon={
                                    <Icon
                                        name="arrow-right"
                                        size={15}
                                        color="white"
                                    />
                                }
                                buttonStyle={styles.button}
                                key={step.id}
                                title={step.name}
                                onPress={() => {
                                    setClickedStep(step.id)
                                }}
                            />
                        )
                    }
                })
            }
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'stretch',
        backgroundColor: '#97b7b7'
    },
    title: {
        textAlign: 'center',
        marginTop: 20,
        fontSize: 20
    },
    info: {
        textAlign: 'center',
        color: '#214342'
    },
    button: {
        backgroundColor: '#214342',
        marginRight: 20,
        marginLeft: 20,
        marginTop: 10
    },
    buttonAddStep : {
        backgroundColor: '#214342',
        marginRight: 20,
        marginLeft: 20,
        marginTop: 10,
        marginBottom: 20
    },
    start : {
        backgroundColor: '#336463',
        marginRight: 40,
        marginLeft: 40,
        height : 60
    }
});

export default Project;
