import React from 'react';
import {StyleSheet, View, Text, TextInput} from 'react-native';
import { Button } from 'react-native-elements';
import { Formik } from 'formik';

import {createProject} from "../../../service/ApiService";
import Datepicker from "../../../components/DatePicker";

const CreateProject = ({ navigation }) => {
    const onSubmit = (value) => {
        value.deadline = new Date(value.year + '-' + value.month + '-' + value.date)
        const data = {
            "name": value.name,
            "deadline": value.deadline
        };
        createProject(data).then(() => navigation.navigate('Home'));
    }

    return (
        <View style={styles.mainContainer}>
            <Text style={styles.title}>
                Créer un projet :
            </Text>
            <Formik
                initialValues={{ name: '', date: 0, month: 0, year: 0 }}
                onSubmit={values => onSubmit(values)}
            >
                {({ handleChange, handleBlur, handleSubmit, values }) => (
                    <View>
                        <Text style={styles.label}>Project Name</Text>
                        <TextInput
                            placeholder="Enter project name..."
                            onChangeText={handleChange('name')}
                            onBlur={handleBlur('name')}
                            value={values.name}
                            style={styles.input}
                        />
                        <Text style={styles.label}>Project Deadline</Text>
                        <Datepicker
                            handleChange={handleChange}
                            handleBlur={handleBlur}
                            values={values}
                        />
                        <Button
                            onPress={handleSubmit}
                            type="clear"
                            disabled={values.name === ''}
                            disabledStyle={styles.disable}
                            buttonStyle={styles.submit}
                            titleStyle={{color: '#fff'}}
                            title="Sauvegarder"
                        />
                    </View>
                )}
            </Formik>
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97b7b7',
        height: '100%',
    },
    title: {
        fontSize: 35,
        marginHorizontal: 10,
        marginBottom: 20,
    },
    input: {
        height: 40,
        margin: 12,
        borderBottomWidth: 1,
        padding: 10,
    },
    inputDate: {
        height: 40,
        margin: 12,
        padding: 10,
    },
    label: {
        fontSize: 24,
        margin: 25,
    },
    submit: {
        marginTop: 30,
        backgroundColor: '#214342',
    },
    disable: {
        color: '#97b7b7',
    },
    inputBorder: {
        width: '30%',
        borderRadius: 8,
        borderColor: '#cacaca',
        borderWidth: 1,
        marginBottom: 20,
    },
});

export default CreateProject;
