import {BACK_URL} from "../env";

export async function requestApi(endPoint, method = "GET", data = false) {
  let body;
  const headers = {
    'accept': "application/json",
    "content-type": "application/json"
  };

  if (data) {
    if (method === "GET") {
      const query = qs.stringify(body);
      endPoint = `${endPoint}?${query}`;
    } else {
      body = JSON.stringify(data);
    }
  }

  // set the right ip with ifconfig (the last ip in 192.168. ...)
  return fetch(BACK_URL + endPoint, {
    method,
    body: body || undefined,
    headers,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return {
        success: true,
        message: "",
        data,
      };
    })
    .catch((error) => {
      const data = {};
      return {
        success: false,
        message: error,
        data,
      };
    });
}

