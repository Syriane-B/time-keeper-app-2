#Back
Made with Strapi
##Dev
```
cd time-keeper-back
npm install
npm run develop
```

#Front
Made with Expo react native
##Dev
Copy env.example.js and custom back url (found with command ifconfig on linux, should start with 192.168...)
```
cd time-keeper-app
npm install
expo start
```
