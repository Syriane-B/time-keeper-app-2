import React from 'react';
import { View } from 'react-native';
import { StyleSheet } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import {FontAwesome} from "@expo/vector-icons";

import {
    useQuery,
    QueryClient,
} from 'react-query';
import {getProjects} from "../service/ApiService";

// Create a client
const queryClient = new QueryClient()

const Home = ({ navigation }) => {
    // Queries
    const { isLoading, data } = useQuery('projects', getProjects);

    if (isLoading) {
        return <View style={styles.container}>
            <Button
                buttonStyle={styles.button}
                title="Loading button"
                loading
            />
        </View>
    }
    return (
        <View style={styles.container}>
            {
                data.data.map(project =>
                    <Button
                        icon={
                            <Icon
                                name="arrow-right"
                                size={15}
                                color="white"
                        />
                        }
                        buttonStyle={styles.button}
                        key={project.id}
                        title={project.name}
                        onPress={() => navigation.navigate('Project', {
                            projectId: project.id
                        })}
                    />
                )
            }
            <Button
                icon={
                    <FontAwesome
                        name="plus"
                        size={35}
                        color={"white"}
                    />
                }
                buttonStyle={styles.button}
                onPress={() => navigation.navigate('CreateProject')}
            />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: '#97b7b7'
    },
    button: {
        backgroundColor: '#214342',
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 20
    }
});

export default Home;
